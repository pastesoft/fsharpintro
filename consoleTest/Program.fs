﻿open pastesoft.com.PredictiveText
open System
// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
[<EntryPoint>]
let main argv =
  (*
  // trying the lib in the console
  let res = Autocomplete "ste" (LoadDict())
  for r in res do
    printfn "%A" r 
  *)

  (*
  // trying the input
  Console.WriteLine("Enter a message and press enter")
  let userInput = System.Console.ReadLine()

  printfn "%A" userInput 

  // looping input 
  while true do 
    printfn "Press any key - to stop it press CTRL + C"
    let keyPressed = Console.ReadKey()
    printfn ""
    printfn "You pressed %A" keyPressed.KeyChar
  *)

  while true do
    Console.WriteLine "Insert the filter to search on the dictionary - PRESS CTRL + C to stops"
    let filter = Console.ReadLine()
    let filtered = LoadDict() |> Autocomplete filter
    for ff in filtered do 
      printfn "%A" ff
  0