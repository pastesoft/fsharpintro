let factorial n =
  let rec inner acc i =
    match i with 
      | 0 -> acc
      | 1 -> acc
      | _ -> inner (acc * i) (i-1)
  inner 1 n

let factorialFold n = 
  [1..n] |> List.fold(*) 1
// val fold : folder:('State -> 'T -> 'State) -> state:'State -> list:'T list -> 'State
// val ( * ) : x:'T1 -> y:'T2 -> 'T3 

factorial 5
factorialFold 5