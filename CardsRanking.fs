type Rank = 
        /// Represents the rank of cards 2 .. 10
        | Value of int
        | Ace
        | King
        | Queen
        | Jack

        /// Discriminated Unions can also implement object-oriented members.
        static member GetAllRanks() = 
            [ yield Ace
              for i in 2 .. 10 do yield Value i
              yield Jack
              yield Queen
              yield King ]

let r = Rank.GetAllRanks()
r.Head    // Ace

List.iter (fun x -> printfn "%A" x) r
r |> List.iter (fun x -> printfn "%A" x)

let d = r.Tail.Head

let rankInt r = 
  match r with 
    | Ace -> 1
    | Value n -> int n
    | Jack -> 10
    | Queen -> 12
    | King -> 13

// convert to all the value
let v = Rank.GetAllRanks() 
        |> List.zip ([1 .. 13])    // List((1, Ace), (2, Value 2), ...)
        |> List.map(fun x -> fst(x))  // List(1, 2, 3, 4, 5, 6,...)


let ranks = Rank.GetAllRanks() 
let av = ranks
          |> List.zip ([1 .. ranks.Length]) 
          |> List.filter(fun x -> snd(x) = Value 121)
          |> List.tryHead
          |> Option.fold (fun _ v -> fst v) 0
          // |> List.map(fun x -> fst(x))

let a = Some (1 ,2)
