// Lambda calculus in F# with recursive type
type Expression = 
  | Var of string
  | Lambda  of Expression * Expression
  | Apply of Expression * Expression

let rec lambdaPrinter lbd =
  match lbd with 
    | Var x -> x
    | Lambda (x, y) -> "λ" + (lambdaPrinter x) + "." + (lambdaPrinter y)
    | Apply (x, y) -> "(" + (lambdaPrinter x) + " " +  (lambdaPrinter y) + ")"

// Identity functiona Lambda(x).x
let lambda1 = Lambda(Var "x", Var "x")

lambda1 |> lambdaPrinter

let funct2 = Lambda(Var "x", 
              Lambda(Var "y", 
                Lambda(Var "z",
                  Apply(Var "x", Apply(Var "z", Var "y"))
                )
              ))
funct2 |> lambdaPrinter // val it : string = "λx.λy.(x  y)"
