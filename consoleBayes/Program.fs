﻿// Test the Bayes classifier through the console

open System
open System.IO
open Argu
open Classifier


// create a discriminated union type
type Arguments = 
  | [<Mandatory>] TrainingData of String   // [<...>] this make this arguments mandatory !!!
  | Message of String
  | Stats
with
  interface IArgParserTemplate with  // comes with Argu, provides an usage string for every union case..  Better message for the user using this command line app
    member s.Usage = 
      match s with
      | TrainingData _ -> "The filename with the data to train the classifier"
      | Message _ -> "My message to classify"
      | Stats _ -> "Show the statistics for the classifier"

[<EntryPoint>]
let main argv = 
  let argParser = ArgumentParser.Create<Arguments>()
  let resArgs = argParser.Parse(argv)

  //let labelData = MakeLabelData "SMSSpamCollection.txt"
  let labelData = MakeLabelData (resArgs.GetResult <@ TrainingData @>)
  // create the corpus and the Bayes classifier based on all the data received.. 
  let corpus = MakeCorpus labelData
  let BayesClassic = MakeClassifier corpus

  if resArgs.Contains <@ Message @> then
    let label = (resArgs.GetResult <@ Message @>) |> BayesClassic
    printfn "%A" label

  // Stats is not mandatory parameter.. so we have to check if available... 
  if resArgs.Contains <@ Stats @> then 
    let training, validation = PartitionRandomly 0.8 labelData
    let corpus = MakeCorpus training
    let BayesClassify = MakeClassifier corpus

    printfn "Training size is %A" training.Length
    printfn "The accuracy is %A " (AccuracyRate Classify validation)
    printfn "The accuracy of our Bayes classifier is %A " (AccuracyRate BayesClassic validation)
    printfn "Total=%A, Ham=%A, Spam=%A" corpus.totalCount corpus.hamCount corpus.spamCount
    printfn "the word 'the' occurs %A in Ham" (corpus.hamFreqs.Item "the")
  
  //labelData |> AccuracyRate Classify



  // let's test the Bayes Classifier
  //let thisbayes = BayesClassify corpus  // change the signature for the function...
  //printfn "The accuracy of our Bayes classifier is %A " (AccuracyRate bayesClassify validation)





  0 // return an integer exit code

