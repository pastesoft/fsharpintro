﻿module Tests
(*


open FsUnit
open NUnit.Framework
open pastesoft.com.PredictiveText

[<Test>]
// it will fail
let testing() =
  [] |> should not' (be Empty)

[<Test>]
// this is ok
let testing2() =
  [0] |> should not' (be Empty)

// let's test the module...

let testDict = LoadDict()

[<Test>]
let shouldLoadDict() =
  testDict |> should not' (be Empty)

[<Test>]
let shouldNotContainTheWordFSharp() =
  testDict |> should not' (contain "fsharp")

[<Test>]
let shouldContainOnlyOneCandidateForAardvarks() =
  let result = testDict |> Autocomplete "aardvarks"
  result |> Array.length |> should equal 1

[<Test>]
let shouldContainElevenCandidatesForTreasu() =
  let result = testDict |> Autocomplete "treasu"
  result |> Array.length |> should equal 11

[<Test>]
let shouldContainNoCandidatesForSteo() =
  let result = testDict |> Autocomplete "steo"
  result |> Array.length |> should be (lessThan 0)

  *)