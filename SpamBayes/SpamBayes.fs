module Classifier

open System
open System.IO

//We need a type that represents a single value at any given time, the Discriminated Union will provide mutually exclusive choices which is the type used to represent the Label.
type Label = Spam | Ham

type Corpus = {
  spamFreqs: Map<string, int>;
  hamFreqs: Map<string, int>;
  spamCount : int;
  hamCount: int;
  totalCount: int;
}

let Classify (message: string) : Label =
  Spam


/// Calculate the accuracy of our model
// The accuracyRate function is a higher order function because it has classifier, a function also, as the first parameter, the second parameter is the pre-labeled SMS data that was downloaded.
let AccuracyRate (classifier: string -> Label) (labelData: seq<Label*string>) : float =
  let numberCorrectedClassified =
    labelData |> Seq.sumBy(fun (label, msg) ->
      match (Classify msg) = label with
      | true -> 1.0
      | false -> 0.0
    )
  numberCorrectedClassified / (float (labelData |> Seq.length))


let dummyData = [ (Ham, "Hello"); (Spam, "Spam") ]


/// Convert Label to string
let String2Label (str:string) =
  let strl = str.ToLower()
  match strl with
  | "ham" -> Ham
  | _ -> Spam


/// Create a random predicate with a treshold to manage how many Spam/ham I want to simulate in the system
let MakeRandomPredicate fractionTrue = // this is a high order function since it will return a function, (predicate)
  let r = System.Random()
  let predicate x =
    r.NextDouble() < fractionTrue
  predicate


/// Generate a list of feature from the input string, simply splitting up the whole input
let Features (msg:string) : seq<string> =
// simplest implementation...
  msg.Split([| ' ' |]) |> Array.toSeq


// number of words for the given label
let CountLabel (label:Label) (labWords : seq<Label*string>) : int =
  labWords
  |> Seq.filter (fun (l,w) -> l = label)  // filter the seq (seq of tuple label*word) with the condition l (label of the element of the seq) = label passed as parameter
  |> Seq.length  // simply count the filtered seq


// calculate the frequencies for the every words for the given 
// and return a Map data structure, where we map the word to its count
// use Seq.groupBy take a key fun or projection, and return a seq with the key and a seq with the elements having that key
let Frequencies (label:Label) (labWords : seq<Label*string>) : Map<string, int> =
  labWords
  |> Seq.filter (fun (l,w) -> l = label)
  |> Seq.groupBy (fun (l,w) -> w)  // result = [("Go", seq [(Ham, "Go"); (Ham, "Go"); (Ham, "Go"); (Ham, "Go"); ...]);
  |> Seq.map (fun(lw, seqWords) -> (lw, Seq.length seqWords)) // result = seq [("Go", 9); ("until", 17); ("jurong", 1); ("point,", 1); ...]
  |> Map.ofSeq

//
// calculate the probabilities ...
//

/// Calculate the probability of having Ham
let ProbHam (corpus:Corpus) : float = 
  (float corpus.hamCount) / (float corpus.totalCount)


/// Calculate the probability of having Spam
let ProbSpam (corpus:Corpus) : float = 
  (float corpus.spamCount) / (float corpus.totalCount)


/// Return a value also if no correspondence are found in the frequencies map
let GetFreqCount (map:Map<string, int>) word =
  match (map |> Map.tryFind word) with 
  | Some(x) -> (float x)
  | _ -> 0.000001   // really low value that we don't manage or need


/// Calculate the probability of a word given Spam - P(WORD|SPAM)
let ProbWordGivenSpam word (corpus:Corpus) : float = 
  (word |> GetFreqCount corpus.spamFreqs) / (float corpus.spamCount)


/// Calculate the probability of a word given Ham - P(WORD | HAM) 
let ProbWordGivenHam word (corpus:Corpus) : float = 
  (word |> GetFreqCount corpus.hamFreqs) / (float corpus.hamCount)


/// Calculate the prob of a word in the whole 
let ProbWord word (corpus:Corpus) : float = 
  (word |> GetFreqCount corpus.hamFreqs) * (ProbHam corpus)+ (word |> GetFreqCount corpus.spamFreqs) * (ProbSpam corpus)
    
/// Calculate the prob of a bunch of words to be Ham...
/// P(Ham|Words) = ((P(W1|Ham) * P(W2|Ham) * ..... * P(Wn|Ham)) x P(Ham)) / (P(w1)*P(w2)*...*P(Wn))
/// or P(Ham|Words) = P(Ham) * For Wi in words P(Wi|Ham) / P(Wi)
let ProbHamGivenWords words (corpus:Corpus) : float = 
  let product = 
    words 
    |> Seq.map (fun w -> (ProbWordGivenHam w corpus) / (ProbWord w corpus))  // for every word w in words map calculate the passed function
    |> Seq.reduce (*) // apply the fun (*) to every pair element - so first element (function) second element, the result (function) third element and so on.. In this case we use the moltiply function 
  
  (ProbHam corpus) * product

/// Calculate the prob of a bunch of words to be Spam...
/// P(Spam|Words) = ((P(W1|Spam) * P(W2|Spam) * ..... * P(Wn|Spam)) x P(Spam)) / (P(w1)*P(w2)*...*P(Wn))
/// or P(Spam|Words) = P(Spam) * For Wi in words P(Wi|Spam) / P(Wi)
let ProbSpamGivenWords words (corpus:Corpus) : float = 
  let product = 
    words 
    |> Seq.map (fun w -> (ProbWordGivenSpam w corpus) / (ProbWord w corpus))  // for every word w in words map calculate the passed function
    |> Seq.reduce (*) // apply the fun (*) to every pair element - so first element (function) second element, the result (function) third element and so on.. In this case we use the moltiply function 
  
  (ProbSpam corpus) * product


/// Calculate the prob of a bunch of words to be in the group specified by <label>...
/// P(<label>|Words) = ((P(W1|<label>) * P(W2|<label>) * ..... * P(Wn|<label>)) x P(<label>)) / (P(w1)*P(w2)*...*P(Wn))
/// or P(<label>|Words) = P(<label>) * For Wi in words P(Wi|<label>) / P(Wi)
let ProbLabelGivenWords words (label:Label) (corpus:Corpus) : float =
  let (ProbWordGiven, ProbLabel) = 
    match label with 
    | Ham -> (ProbWordGivenHam, ProbHam)
    | Spam -> (ProbWordGivenSpam, ProbSpam)
  
  let product = 
    words 
    |> Seq.map (fun w -> (ProbWordGiven w corpus) / (ProbWord w corpus))  // for every word w in words map calculate the passed function
    |> Seq.reduce (*) // apply the fun (*) to every pair element - so first element (function) second element, the result (function) third element and so on.. In this case we use the moltiply function 
  
  (ProbLabel corpus) * product



// test ...
//printfn "%A" ((MakeRandomPredicate 0.9)())

/// Create the Labeled seq of the data found in the input file 
let MakeLabelData fileName = 
  let myDataFile = Path.Combine(__SOURCE_DIRECTORY__, fileName)
  let lines = myDataFile |> File.ReadAllLines

  /// For every array lines, let's split it and return the result inside a Tuple
  let splitDataLines =
    lines
      |> Seq.map(fun str -> str.Split [|'\t'|])  // with map we read every lines and then we split with tab (\t)
      |> Seq.map(fun stra -> (String2Label stra.[0]), stra.[1])

  splitDataLines 

// Make a training collection of data with a random fraction % of the data
let PartitionRandomly fraction labelData = 
  let training, validation =
    labelData
    |> Seq.toList
    |> List.partition (MakeRandomPredicate fraction)

  (training, validation)


/// Transform the data and create the Corpus with the main freqs stored
let MakeCorpus data =
  // create intermediate values ...
  // Convert the string with the whole messge in a seq of string (one for each word in the message)
  // and return the sequence in a tuple with the label associated to the message
  let words : seq<Label*seq<string>> =
    data
    |> Seq.map (fun (label, msg) -> (label, Features msg))

  // take a seq of tuple with Label and string (all the words in the msg) and returns
  // a seq of tuple with label and string that is the mapping of the label to each word in the msg (the seq of string in input)
  // the collect function applies the function passed to each element of the seq and then concatenates all the results together
  let labeledWords : seq<Label*string> =
    words
    |> Seq.collect (fun(aLabel, ws) -> 
    Seq.map (fun w -> (aLabel,w)) ws)  // mapping for every string (words) in ws and returns a tuple of label and word and run this map on ws
// Correct. Seq.collect will take a sequence of sequences and extract all the individual items within the sequences and create a single flattened sequence containing all the individual items.


  let corpus = {
    spamFreqs = labeledWords |> Frequencies Spam;
    hamFreqs = labeledWords |> Frequencies Ham;
    spamCount = labeledWords |> CountLabel Spam;
    hamCount = labeledWords |> CountLabel Ham;
    totalCount = labeledWords |> Seq.length;
  }
  corpus

/// Create and return the function for the Bayes classifier...
let MakeClassifier corpus = 
  /// Bayes classifier...
  let BayesClassify (msg:string) : Label = 
    let seqWord = Features msg
    let pHam = ProbHamGivenWords seqWord corpus
    let pSpam = ProbSpamGivenWords seqWord corpus
    printfn "Ham=%A" pHam
    printfn "Spam=%A" pSpam
    if pHam > pSpam then Ham else Spam
  BayesClassify

// **********************************
// **********************************
// **********************************

// Additional exercise ... Find the frequencies based on the length of the word
let FrequenciesByLength (label:Label) (labWords : seq<Label*string>) =
  labWords
  |> Seq.filter (fun (l,w) -> l = label)
  |> Seq.map (fun (l,w) -> w)  // seq of all the words (number label) =  seq ["Go"; "until"; "jurong"; "point,"; ...]
  |> Seq.groupBy (fun (w) -> (w |> String.length)) // result = seq [(2, seq ["Go"; "in"; ...]);(5, seq ["until"; "bugis"; ...]);(6, seq ["jurong"; "point,"; ...]);
  |> Map.ofSeq
  // result 
  // map
  //  [(0, seq [""; ""; ""; ""; ...]);
  //   (1, seq ["n"; "e"; "U"; "U"; ...]);
  //   (2, seq ["Go"; "in"; "la"; "so"; ...]);
  //   (3, seq ["got"; "dun"; "say"; "Nah"; ...]);
  //   (4, seq ["only"; "Cine"; "then"; "goes"; ...]);
  //   (5, seq ["until"; "bugis"; "great"; "world"; ...]);