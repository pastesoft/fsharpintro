let mystr : string = "this is a string"
let my2 = "this is another string"
let aint: int = 123

let add x y = x + y
let add3 = add 3
let addCurry = fun x -> fun y -> x + y

let sum x y = x + y
sum 3 3
let sum2 = fun x -> fun y -> x + y  // same of sum 
let rs = sum2 3 // int -> int
rs 6  // int = 9
let rs2 = sum 3
rs2 6


let l1 = [1; 2; 3]
let l2 = [4; 5; 6]
let rl = l1 @ l2
// function composition and piping...
let isEven x = x%2 = 0
 List.map(fun x -> x*x) (List.filter isEven rl)
// or I can rewrite the same using the pipi operator...
List.filter(fun y -> y % 2 = 0) rl |> List.map(fun x -> x * x)
List.map(fun x -> if (x%2=0) then x*x else 0) rl

let t = ("cats", "dogs", System.Math.PI, 42, "C#", "Java")
let a1, a2, _, _, _, _ = t 
let _, _, pi, _, _, _ = t 
// also string can be accessed as a array of char
let pstr = "Hello, World!!!"
printfn "%c" pstr.[7] // w
let oneTo100 = [|1..100|]
let justThree = oneTo100.[0..2] // [|1;2;3|]
let other3 = oneTo100.[60..62] // [|61;62;63|]

let callIt3Times n (f:int -> int) = f n |> f |> f // sama as f(f(f(n)))
let callIt3TImesGen f n = f n |> f |> f // sama as f(f(f(n)))  // generic one
callIt3TImesGen (fun x -> x + x) 10  // 80
let tothepower8 = callIt3TImesGen (fun x -> x*x)  // x*x = x^2 ... so call it three times means (((x^2)^2)^2)
tothepower8 2 // 256
2 |> tothepower8  // 256 ... the same.. just more expressive.. :) 

let oneTo100 = [|1..100|]
let justThree = oneTo100.[0..2] // [|1;2;3|]
let other3 = oneTo100.[60..62] // [|61;62;63|]
other3 |> Array.map(fun x -> x * x)
other3 |> Array.fold(fun acc x -> acc * x)(1)

let sumOfSquare l = 
  l |> List.map(fun x -> x * x) |> List.sum
  // List.sum(List.map(fun x -> x * x) l) //  c# style..
  // l |> List.map(fun x -> x * x) |> List.fold(fun acc x -> acc + x) 0   // the same but with fold
[1..100] |> sumOfSquare

let firstNames = [| "Leonard"; "Sheldon"; "Howard"; "Penny"; "Raj"; "Bernadette"; "Amy" |]
let lastNames = [| "Hofstadter"; "Cooper"; "Wolowitz"; ""; "Koothrappali"; "Rostenkowski"; "Fowler" |]
let fullNames = lastNames |> Array.zip firstNames
=======
let sumOfSquare l = 
  l |> List.map(fun x -> x * x) |> List.sum
  // List.sum(List.map(fun x -> x * x) l) //  c# style..
  // l |> List.map(fun x -> x * x) |> List.fold(fun acc x -> acc + x) 0   // the same but with fold
[1..100] |> sumOfSquare

let valid = Some 12
let invalid = None

let lv = [
  valid
  invalid ]

let rec checkAList = function
  | [] -> "That's all folks!!!"
  | hd :: tl -> match hd with §§
                    | Some x -> printfn "%A" x
                    | None -> printfn "none"
                checkAList tl

checkAList lv

type Person = {
  Name: string
  Surname: string
}

type Employee =
  | Worker of Person
  | Manager of Employee list

let aOne = {Name="uno";Surname="due"}
let w1 = Worker aOne
let mgr = Manager [w1; w1]

let isEven x = 
  match x%2 with
    | 0 -> Some x
    | _ -> None

//let rnd = System.Random()
//let rec randomWalk x = seq { yield x
//                             yield! randomWalk (x + rnd.NextDouble() - 0.5) }
let rec randomWalk x = 
  seq { yield x
        yield! randomWalk (x + 1) }

randomWalk 5
  |> Seq.truncate 10 
  |> Seq.toList   // val it : int list = [5; 6; 7; 8; 9; 10; 11; 12; 13; 14]

let squareIt x = x * x

[1..10] |> List.map isEven |> List.map squareIt
// def flatMap[B](f: A => Option[B]) : Option[B]
type M<'A>
let flatMap<A, B>(f: A -> M<B>): M<B>


factorial 6
let compare x (f: 'a -> bool) : bool = x |> f
