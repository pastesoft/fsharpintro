﻿// new script file for the new course of F#: releasing a F# lib
open System
open System.IO

let myfilePath = Path.Combine(__SOURCE_DIRECTORY__, "dict.txt")

let readDictFromPath path = 
  File.ReadAllLines path

let loadDict () =
  let dict = readDictFromPath myfilePath
  dict

//printfn "%A" (loadDict())
let loaded = loadDict()
printfn "%A" loaded

// filtering...
//example
let filtering filter data =  
  let data = [| "bbfgbfd";"bghfhdhg";"bwana";"bylaw"|]
  let candidates = data |> Array.filter(fun word -> word.Contains "w")
  candidates


// our proper code...

let autocomplete (filter:string) (data : string[]) =  
  let candidates = data |> Array.filter(fun word -> word.StartsWith filter)
  candidates

// test it 
let filtered = loaded |> autocomplete "s"
filtered.Length