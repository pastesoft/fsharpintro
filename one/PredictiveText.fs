﻿module pastesoft.com.PredictiveText

open System
open System.IO

let private myfilePath = Path.Combine(__SOURCE_DIRECTORY__, "dict.txt")

/// Allows you to load your dictionary
let LoadDictFromPath path = 
  File.ReadAllLines path

/// Loads the default dictionary 
let LoadDict () =
  let dict = LoadDictFromPath myfilePath
  dict

// remove for the library purpose
//let loaded = loadDict()
//printfn "%A" loaded

/// This function will find the words that start with the given prefix
let Autocomplete (filter:string) (data : string[]) =  
  let candidates = data |> Array.filter(fun word -> word.StartsWith filter)
  candidates

// test it 
//let filtered = loaded |> autocomplete "s"
//filtered.Length