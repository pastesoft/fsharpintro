﻿let myseq = seq { 
  for x in 1 .. 100 ->
    if x % 3 = 0 && x % 5 = 0 then "FizzBuzz"
    elif x % 3 = 0 then "Fizz"
    elif x % 5 = 0 then "Buzz"
    else x.ToString()
   }

myseq |> Seq.length

Seq.iter (fun s -> printfn "%s" s) myseq

// https://projecteuler.net/problem=1
//
// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
//
// Find the sum of all the multiples of 3 or 5 below 1000.
//
  
let euler1 = 
  seq {
    for i in 1 .. 999 do 
      if i % 3 = 0 || i % 5 = 0 then yield i 
  }

let resEuler = euler1 |> Seq.fold (fun a b -> a + b) 0

let res2 = euler1 |> Seq.reduce (fun x num -> x + num)
