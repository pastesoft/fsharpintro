type MyOptionBuilder () =
  member x.Bind(m, f) =
    match m with
    | Some value -> f value
    | _ -> None

  member x.Return value = 
    Some value


let myoption = MyOptionBuilder()

let maybeAdd mA mB =
  myoption {
    let! a = mA
    let! b = mB
    return a + b
  }

let x = Some(1)
let y : int option = None
let aa = maybeAdd x y 